const fs = require('fs')
const path = require('path')


/**
 * 
 * @param {String} pathToProject - path
 * @param {String} Options.dirs - блоклиск
 * @param {String} Options.files - блоклиск
 * @param {String} Options.types - блоклиск по типу (расширению)
 * @returns {Object} @status - status; @text - answer; @err - err
 */
async function start(pathToProject, {dirs=[], files=[], types=[]}) {
    try {
        const counter = {}
        const mainDir = await fs.readdirSync(pathToProject)

        // перебираем все файлы и папки дирректории
        for (let i = 0; i < mainDir.length; i++) {
            const elName = mainDir[i]
            const elPath = path.join(pathToProject, elName)
            const checkDir = await isDir(elPath)
            // console.log(`elName: ${elName}; checkDir: ${checkDir}`)

            // если дирректория то проверям есть ли она в блок листе + запускаем рекурсию
            if (checkDir && !(new Set(dirs)).has(elName)) {
                const getStart = await start(path.join(pathToProject, elName), {dirs, files, types})
                if (!getStart.status) throw getStart
                const newCounter = getStart.counter

                // теперь нужно соеденить обьекты
                for (let extname in newCounter) {
                    const dataCounter = newCounter[extname]

                    if ( !counter[extname] ) { // если нет такого расширения
                        counter[extname] = {
                            count: dataCounter.count,
                            letterCount: dataCounter.letterCount,
                            lineCount: dataCounter.lineCount
                        }
                    } else { // если есть такое расширение
                        counter[extname].count += dataCounter.count
                        counter[extname].letterCount += dataCounter.letterCount
                        counter[extname].lineCount += dataCounter.lineCount
                    }
                }
                // если файл то проверяем есть ли он в блок листе + добавляем инфу
            } else if ( !checkDir && !isBlockFile(files, types, elName) ) {
                const fileData = await fs.readFileSync(elPath).toString()
                const letterCount = (fileData || '').length
                const lineCount = (fileData.match(/\n/g) || '').length

                const extname = path.extname(elName).slice(1) // берем расширение и убираем точку
                if ( !counter[extname] ) {
                    counter[extname] = {letterCount, lineCount, count: 1}
                } else {
                    counter[extname].count += 1
                    counter[extname].letterCount += letterCount
                    counter[extname].lineCount += lineCount
                }
            }
        }

        return {
            status: true,
            counter
        }
    } catch(err) {
        return {
            status: false,
            err
        }
    }
}


async function isDir(pathToDir) {
    try {
        await fs.readdirSync(pathToDir)
        return true
    } catch(err) {
        return false
    }
}


function isBlockFile(files, types, checkFile) { // true если заблокирован
    // console.log(`checkFile: ${checkFile} | result: ${new Set(files).has(checkFile) || !types.every(t => !checkFile.endsWith(t))}`)
    return new Set(files).has(checkFile) || !types.every(t => !checkFile.endsWith(t))
}


start(
    // '/home/myname/Work/Pal-Bot/',
    '/home/myname/Work/bot-server/',
    // '/home/myname/Work/shop-bot-site/',
    // '/home/myname/Work/testdel/botserver/bot-server/',
    // '/home/myname/Work/testdel/botserver/shop-bot-site/',
    {
        dirs: [ '.git', 'node_modules', 'logs', 'font', 'img', 'video', 'champions', 'dist' ],
        types: [ 'git', 'json', 'md', 'drawio', 'png', 'ico', 'map' ],
        files: [ '.directory', 'zip', 'ttf', '.gitignore', 'babel.config', 'vue.config.js', 'emojis.js', 'LICENSE', '.env' ]
    }
).then(console.log)
